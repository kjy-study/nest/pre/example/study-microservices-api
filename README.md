
# Micro Services Project

마이크로 서비스는 스탠다드 프로젝트보단 monorepo 프로젝트에 적용하는 것이 더 적합합니다. 그러므로 monorepo 프로젝트를 기준으로 셋팅합니다.

### 프로젝트 생성
```bash
# 프로젝트 생성
nest new study-microservices-api

# monorepo 프로젝트로 변경
nest g app serviceA
nest g app serviceB

# 기본 프로젝트는 삭제 합니다.
rm -rf apps/study-microservices-api
```

### nest-cli.json 변경
* AS-IS
```json
{
  "collection": "@nestjs/schematics",
  "sourceRoot": "apps/study-microservices-api/src",
  "monorepo": true,
  "root": "apps/study-microservices-api",
  "compilerOptions": {
    "webpack": true,
    "tsConfigPath": "apps/study-microservices-api/tsconfig.app.json"
  },
  "projects": {
    "study-microservices-api": {
      "type": "application",
      "root": "apps/study-microservices-api",
      "entryFile": "main",
      "sourceRoot": "apps/study-microservices-api/src",
      "compilerOptions": {
        "tsConfigPath": "apps/study-microservices-api/tsconfig.app.json"
      }
    },
    "service-a": {
      "type": "application",
      "root": "apps/service-a",
      "entryFile": "main",
      "sourceRoot": "apps/service-a/src",
      "compilerOptions": {
        "tsConfigPath": "apps/service-a/tsconfig.app.json"
      }
    },
    "service-b": {
      "type": "application",
      "root": "apps/service-b",
      "entryFile": "main",
      "sourceRoot": "apps/service-b/src",
      "compilerOptions": {
        "tsConfigPath": "apps/service-b/tsconfig.app.json"
      }
    }
  }
}
```
* TO-BE   
삭제 : sourceRoot, root, compilerOptions, projects/study-microservices-api
```json

{
  "collection": "@nestjs/schematics",
  "monorepo": true,
  "projects": {
    "service-a": {
      "type": "application",
      "root": "apps/service-a",
      "entryFile": "main",
      "sourceRoot": "apps/service-a/src",
      "compilerOptions": {
        "tsConfigPath": "apps/service-a/tsconfig.app.json"
      }
    },
    "service-b": {
      "type": "application",
      "root": "apps/service-b",
      "entryFile": "main",
      "sourceRoot": "apps/service-b/src",
      "compilerOptions": {
        "tsConfigPath": "apps/service-b/tsconfig.app.json"
      }
    }
  }
}

```

### 마이크로서비스 구축
```bash
# 마이크로서비스 패키지 설치
npm i --save @nestjs/microservices

```

* 서비스 A의 main.ts를 http + microservice로 실행할 수 있도록 수정합니다.
```typescript
// main.ts
import { NestFactory } from '@nestjs/core';
import { ServiceAModule } from './service-a.module';
import { TcpOptions, Transport } from '@nestjs/microservices';

async function bootstrap() {
  const app = await NestFactory.create(ServiceAModule);

  // // Setup tcp server for api
  const options: TcpOptions = {
    transport: Transport.TCP,
    options: {
      host: '127.0.0.1',
      port: 8001,
    },
  };
  app.connectMicroservice(options);
  app.startAllMicroservices();

  // http security checker
  // https://github.com/helmetjs/helmet#how-it-works
  // app.use(helmet());
  // app.use(cookieParser());

  await app.listen(3001);
}
bootstrap();
```

* 서비스 A의 controller에 MessagePattern, EventPattern을 추가 합니다.   
위 2개의 패턴은 다른 API 클라이언트로부터 메시지와, 이벤트를 수신할 수 있도록 채널을 오픈 시킵니다.

```typescript
// service-a.controller.ts
import { Controller, Get } from '@nestjs/common';
import { ServiceAService } from './service-a.service';
import { EventPattern, MessagePattern } from '@nestjs/microservices';

@Controller()
export class ServiceAController {
  constructor(private readonly serviceAService: ServiceAService) {}

  @Get()
  async httpHello() {
    return 'http - hello';
  }

  @MessagePattern('getHello')
  async getHello(name: string): Promise<string> {
    return this.serviceAService.getHello(name);
  }

  @MessagePattern({ cmd: 'sum' })
  async accumulate(data: number[]): Promise<number> {
    return (data || []).reduce((a, b) => a + b);
  }

  /**
   * 메시지 패턴
   * emit 함수로 호출할 수 있습니다.
   * 특정 조건일 발생했을 때 다른 서비스에 단순히 알리는 목적으로 사용 합니다.
   * @param data
   */
  @EventPattern('user_created')
  async handleUserCreated(data: Record<string, unknown>) {
    await this.serviceAService.handleUserCreated(data);
  }
}

```

* 서비스 B에서 서비스 A의 메시지(or 이벤트)를 호출할 수 있도록 셋팅 합니다.
```typescript
// service-b.module.ts
import { Module } from '@nestjs/common';
import { ServiceBController } from './service-b.controller';
import { ServiceBService } from './service-b.service';
import { ClientsModule, Transport } from '@nestjs/microservices';

@Module({
  imports: [
    ClientsModule.register([
      {
        name: 'SERVICE_A',
        transport: Transport.TCP,
        options: {
          host: '127.0.0.1',
          port: 8001,
        },
      },
    ]),
  ],
  controllers: [ServiceBController],
  providers: [ServiceBService],
})
export class ServiceBModule {}

```
```typescript
// service-b.service.ts
import { Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { lastValueFrom } from 'rxjs';

@Injectable()
export class ServiceBService {
  constructor(
    @Inject('SERVICE_A') private readonly clientServiceA: ClientProxy,
  ) {}

  async getHello(): Promise<string> {
    const pattern = 'getHello';
    const payload = 'mion';
    const message = await lastValueFrom(
      this.clientServiceA.send<string, string>(pattern, payload),
    );
    return message;
  }

  async accumulate(): Promise<number> {
    const pattern = { cmd: 'sum' };
    const payload = [10, 20, 30];
    const message = await lastValueFrom(
      this.clientServiceA.send<number, object>(pattern, payload),
    );
    return message;
  }

  async createUser() {
    // DB에 사용자 저장하고...

    // 서비스 A에 이벤트를 보낸다.
    const pattern = 'user_created';
    const payload = {
      user_id: 'mion',
      email: 'mion@gmail.com',
    };
    this.clientServiceA.emit(pattern, payload);
  }
}

```

```typescript
// service-b.controller.ts
import { Controller, Get } from '@nestjs/common';
import { ServiceBService } from './service-b.service';

@Controller()
export class ServiceBController {
  constructor(private readonly serviceBService: ServiceBService) {}

  @Get()
  async getHello(): Promise<string> {
    return await this.serviceBService.getHello();
  }

  @Get('/accumulate')
  async getAccumulate(): Promise<number> {
    return await this.serviceBService.accumulate();
  }

  @Get('/user-create')
  async create() {
    await this.serviceBService.createUser();
  }
}


```

### 마이크로서비스 실행
```bash
# 서비스 A 실행
nest start service-a

# 서비스 B 실행
nest start service-b


# 서비스 A 개발환경 실행
nest start service-a --debug --watch
# 서비스 B 개발환경 실행
nest start service-b --debug --watch
```

* 이제 서비스 B에서 API를 호출하여 결과를 확인 합니다.
