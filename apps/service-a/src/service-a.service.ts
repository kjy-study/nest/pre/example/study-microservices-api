import { Injectable } from '@nestjs/common';

@Injectable()
export class ServiceAService {
  getHello(name: string): string {
    return `${name} - serviceA Hello World!`;
  }

  async handleUserCreated(data: Record<string, unknown>) {
    console.log('Service-A 사용자 생성 이벤트 수신');
  }
}
