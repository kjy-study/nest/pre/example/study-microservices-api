import { Controller, Get } from '@nestjs/common';
import { ServiceAService } from './service-a.service';
import { EventPattern, MessagePattern } from '@nestjs/microservices';

@Controller()
export class ServiceAController {
  constructor(private readonly serviceAService: ServiceAService) {}

  @Get()
  async httpHello() {
    return 'http - hello';
  }

  @MessagePattern('getHello')
  async getHello(name: string): Promise<string> {
    return this.serviceAService.getHello(name);
  }

  @MessagePattern({ cmd: 'sum' })
  async accumulate(data: number[]): Promise<number> {
    return (data || []).reduce((a, b) => a + b);
  }

  /**
   * 메시지 패턴
   * emit 함수로 호출할 수 있습니다.
   * 특정 조건일 발생했을 때 다른 서비스에 단순히 알리는 목적으로 사용 합니다.
   * @param data
   */
  @EventPattern('user_created')
  async handleUserCreated(data: Record<string, unknown>) {
    await this.serviceAService.handleUserCreated(data);
  }
}
