import { NestFactory } from '@nestjs/core';
import { ServiceAModule } from './service-a.module';
import { TcpOptions, Transport } from '@nestjs/microservices';

async function bootstrap() {
  const app = await NestFactory.create(ServiceAModule);

  // // Setup tcp server for api
  const options: TcpOptions = {
    transport: Transport.TCP,
    options: {
      host: '127.0.0.1',
      port: 8001,
    },
  };
  app.connectMicroservice(options);
  app.startAllMicroservices();

  // http security checker
  // https://github.com/helmetjs/helmet#how-it-works
  // app.use(helmet());
  // app.use(cookieParser());

  await app.listen(3001);
}
bootstrap();
