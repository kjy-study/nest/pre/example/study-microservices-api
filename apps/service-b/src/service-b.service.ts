import { Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { lastValueFrom } from 'rxjs';

@Injectable()
export class ServiceBService {
  constructor(
    @Inject('SERVICE_A') private readonly clientServiceA: ClientProxy,
  ) {}

  async getHello(): Promise<string> {
    const pattern = 'getHello';
    const payload = 'mion';
    const message = await lastValueFrom(
      this.clientServiceA.send<string, string>(pattern, payload),
    );
    return message;
  }

  async accumulate(): Promise<number> {
    const pattern = { cmd: 'sum' };
    const payload = [10, 20, 30];
    const message = await lastValueFrom(
      this.clientServiceA.send<number, object>(pattern, payload),
    );
    return message;
  }

  async createUser() {
    // DB에 사용자 저장하고...

    // 서비스 A에 이벤트를 보낸다.
    const pattern = 'user_created';
    const payload = {
      user_id: 'mion',
      email: 'mion@gmail.com',
    };
    this.clientServiceA.emit(pattern, payload);
  }
}
