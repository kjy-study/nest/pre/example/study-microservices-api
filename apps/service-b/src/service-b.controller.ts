import { Controller, Get } from '@nestjs/common';
import { ServiceBService } from './service-b.service';

@Controller()
export class ServiceBController {
  constructor(private readonly serviceBService: ServiceBService) {}

  @Get()
  async getHello(): Promise<string> {
    return await this.serviceBService.getHello();
  }

  @Get('/accumulate')
  async getAccumulate(): Promise<number> {
    return await this.serviceBService.accumulate();
  }

  @Get('/user-create')
  async create() {
    await this.serviceBService.createUser();
  }
}
